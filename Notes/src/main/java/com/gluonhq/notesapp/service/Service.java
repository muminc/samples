package com.gluonhq.notesapp.service;

import com.gluonhq.charm.connect.GluonClient;
import com.gluonhq.charm.connect.GluonClientBuilder;
import com.gluonhq.charm.connect.service.CharmObservableList;
import com.gluonhq.charm.connect.service.CharmObservableObject;
import com.gluonhq.charm.connect.service.StorageService;
import com.gluonhq.charm.connect.service.StorageWhere;
import com.gluonhq.charm.connect.service.SyncFlag;
import com.gluonhq.notesapp.model.Note;
import com.gluonhq.notesapp.model.Settings;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javax.annotation.PostConstruct;

public class Service {
    
    private static final String NOTES = "notes v2.1";
    
    private static final String NOTES_SETTINGS = "notes-settings";
    
    private final ListProperty<Note> notes = new SimpleListProperty<>(FXCollections.observableArrayList());
    
    private final ObjectProperty<Settings> settings = new SimpleObjectProperty<>(new Settings());
    
    private GluonClient gluonClient;
    private StorageService userStorage;
    
    @PostConstruct
    public void postConstruct() {
        gluonClient = GluonClientBuilder.create().build();
        userStorage = gluonClient.getStorageService();
    }
    
    public void retrieveNotes() {
        CharmObservableList<Note> charmNotes = userStorage.<Note>retrieveList(NOTES,
                Note.class, StorageWhere.DEVICE, 
                SyncFlag.LIST_WRITE_THROUGH, SyncFlag.OBJECT_WRITE_THROUGH);
        
        charmNotes.stateProperty().addListener((obs, ov, nv) -> {
            if (nv.equals(CharmObservableList.State.INITIALIZED)) {
                notes.set(charmNotes);
                
                retrieveSettings();
            }
        });
    }
    
    public Note addNote(Note note) {
        notes.get().add(note);
        return note;
    }

    public void removeNote(Note note) {
        notes.get().remove(note);
    }

    public ListProperty<Note> notesProperty() {
        return notes;
    }
    
    private void retrieveSettings() {
        CharmObservableObject<Settings> charmSettings = userStorage.<Settings>retrieveObject(NOTES_SETTINGS, 
                Settings.class, StorageWhere.DEVICE);
        charmSettings.stateProperty().addListener((obs, ov, nv) -> {
            if (nv.equals(CharmObservableObject.State.INITIALIZED) && charmSettings.get() != null) {
                settings.set(charmSettings.get());
            }
        });
    }
    
    public void storeSettings() {
        userStorage.<Settings>storeObject(NOTES_SETTINGS, settings.get(), StorageWhere.DEVICE);
    }
    
    public ObjectProperty<Settings> settingsProperty() {
        return settings;
    }
    
}
