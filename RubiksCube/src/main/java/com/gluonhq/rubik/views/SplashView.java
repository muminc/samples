package com.gluonhq.rubik.views;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.LifecycleEvent;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.rubik.GluonRubik;
import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class SplashView extends View {

    public SplashView(String name) {
        super(name);
        
        Label access = new Label("Loading...");
        access.setTranslateY(200);
        access.setVisible(false);
        
        StackPane pane = new StackPane();
        pane.getStyleClass().add("pane");
        pane.setMaxSize(300, 300);
        setCenter(new StackPane(pane, access));

        Task<Void> task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                Platform.runLater(() -> {
                    MobileApplication.getInstance().switchView(GluonRubik.RUBIK_VIEW);
                    // Get rid of splash view
                    MobileApplication.getInstance().removeViewFactory(GluonRubik.SPLASH_VIEW);
                });
                return null;
            }
        };

        addEventHandler(LifecycleEvent.SHOWN, e -> {
            PauseTransition pause = new PauseTransition(Duration.seconds(2));
            pause.setOnFinished(f -> {
                access.setVisible(true);
                new Thread(task).start();
            });
            pause.play();
        });

        getStylesheets().add(SplashView.class.getResource("splash.css").toExternalForm());
    }

    @Override
    protected void updateAppBar(AppBar appBar) {
        appBar.setVisible(false);
    }

}
