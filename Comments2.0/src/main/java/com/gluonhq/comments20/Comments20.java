package com.gluonhq.comments20;

import com.gluonhq.charm.down.common.PlatformFactory;
import com.gluonhq.comments20.views.CommentsView;
import com.gluonhq.comments20.views.EditionView;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.Avatar;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.control.NavigationDrawer.Item;
import com.gluonhq.charm.glisten.layout.layer.SidePopupView;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import com.gluonhq.comments20.cloud.Service;
import com.gluonhq.comments20.views.CommentsPresenter;
import javafx.beans.value.ChangeListener;

public class Comments20 extends MobileApplication {

    public static final String PRIMARY_VIEW = HOME_VIEW;
    public static final String SECONDARY_VIEW = "Edition View";
    public static final String MENU_LAYER = "Side Menu";
    
    private CommentsPresenter commentsPresenter;
    private Avatar avatar;
    
    private Item primaryItem;
    private Item secondaryItem;

    private final ChangeListener listener = (obs, oldItem, newItem) -> {
            hideLayer(MENU_LAYER);
            switchView(newItem.equals(primaryItem) ? PRIMARY_VIEW : SECONDARY_VIEW);
        };

    @Override
    public void init() {
        addViewFactory(PRIMARY_VIEW, () -> {
            final CommentsView commentsView = new CommentsView();
            commentsPresenter = (CommentsPresenter) commentsView.getPresenter();
            return (View) commentsView.getView();
        });
        addViewFactory(SECONDARY_VIEW, () -> (View) new EditionView().getView());
        
        NavigationDrawer drawer = new NavigationDrawer();
        avatar = new Avatar(21);
        
        NavigationDrawer.Header header = new NavigationDrawer.Header("Gluon Mobile",
                "The Comments App", avatar);
        drawer.setHeader(header);
        
        primaryItem = new Item("Comments", MaterialDesignIcon.COMMENT.graphic());
        secondaryItem = new Item("Edition", MaterialDesignIcon.EDIT.graphic());
        drawer.getItems().addAll(primaryItem, secondaryItem);
        
        primaryItem.setSelected(true);
        drawer.selectedItemProperty().addListener(listener);
        
        addLayerFactory(MENU_LAYER, () -> new SidePopupView(drawer));
        
        viewProperty().addListener((obs, ov, nv) -> {
            drawer.selectedItemProperty().removeListener(listener);
                if (nv.getName().equals(PRIMARY_VIEW)) {
                    primaryItem.setSelected(true);
                    secondaryItem.setSelected(false);
                    drawer.setSelectedItem(primaryItem);
                } else {
                    primaryItem.setSelected(false);
                    secondaryItem.setSelected(true);
                    drawer.setSelectedItem(secondaryItem);
                }
                drawer.selectedItemProperty().addListener(listener);
            });
        
    }

    @Override
    public void postInit(Scene scene) {
        Swatch.BLUE.assignTo(scene);
        
        scene.getStylesheets().add(Comments20.class.getResource("style.css").toExternalForm());
        
        ((Stage) scene.getWindow()).getIcons().add(new Image(Comments20.class.getResourceAsStream("/icon.png")));
        
        secondaryItem.disableProperty().bind(commentsPresenter.userProperty().isNull());
        
        commentsPresenter.userProperty().addListener((obs, ov, nv) -> avatar.setImage(getAvatarImage()));
        avatar.setImage(getAvatarImage());
        if (PlatformFactory.getPlatform().isTablet()) {
            avatar.getStyleClass().add("tablet");
        }
    }
    
    private Image getAvatarImage() {
        if (commentsPresenter != null && commentsPresenter.userProperty().get() != null) {
            return Service.getUserImage(commentsPresenter.userProperty().get().getPicture());
        } 
        return new Image(Comments20.class.getResourceAsStream("/icon.png"));
    }
    
}
